# Øvingstekster

Denne mappen inneholder øvingstekster for TDT4100 - Objektorientert programmering våren 2023. Tabellen under inneholder linker til hver enkelt oppgavetekst og tema for øvingen. Linker vil bli oppdatert underveis.

| Øving                         | Tema                               |
| ----------------------------- | ---------------------------------- |
| [Øving 0](./oving0/README.md) | Introduksjon og oppsett av Java    |
| [Øving 1](./oving1/README.md) | Java-syntaks og objektorientert tankegang |
| [Øving 2](./oving3/README.md) | Innkapsling og validering|
| [Øving 3](./oving3/README.md)| Klasser og testing|
